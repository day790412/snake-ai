import stable_baselines3 as sb3

from main.GuessNumberEnv import GuessNumberEnv

# Load the trained model
model = sb3.PPO.load("guess_number_model")

# Create a new environment
env = GuessNumberEnv()

# Reset the environment
obs = env.reset()

# Let's try it for a few steps
for i in range(50):
    # The model predicts the action to take
    action, _ = model.predict(obs)
    # The action is applied in the environment
    obs, reward, done, info = env.step(action)
    print(f"Step {i + 1}:")
    print(f"Action taken: {env._decode(action)}")
    print(f"Reward received: {reward}")
    print(f"New observation: {obs}")
    if done:
        print("Game over!")
        break
