import random

import gym
import numpy as np

class GuessNumberEnv(gym.Env):
    def __init__(self):
        super(GuessNumberEnv, self).__init__()
        self.target = None
        self.action_space = gym.spaces.Discrete(10000)  # For 4-digit numbers
        self.observation_space = gym.spaces.Box(low=0, high=4, shape=(2,), dtype=int)

    def step(self, action):
        guess = self._decode(action)
        A, B = self._score(guess, self.target)
        reward = A * 2 + B
        if A == 4:
            done = True
        else:
            done = False
        return np.array([A, B]), reward, done, {}

    def reset(self):
        self.target = self._generate_number()
        return np.array([0, 0])

    def _generate_number(self):
        return str(random.randint(0, 9999)).zfill(4)

    def _decode(self, action):
        return str(action).zfill(4)

    def _score(self, guess, target):
        A = sum(a==b for a, b in zip(guess, target))
        B = sum((g in target) for g in guess) - A
        return A, B

