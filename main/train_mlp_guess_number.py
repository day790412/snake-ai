import os

from main.GuessNumberEnv import GuessNumberEnv
import stable_baselines3 as sb3
NUM_ENV = 32
LOG_DIR = "logs"
os.makedirs(LOG_DIR, exist_ok=True)


# 用 PPO 算法訓練模型
def main():
    env = GuessNumberEnv()
    model = sb3.PPO("MlpPolicy", env, verbose=1)
    model.learn(total_timesteps=30000)
    model.save("guess_number_model")


if __name__ == "__main__":
    main()
